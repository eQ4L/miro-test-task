export interface IEmailItem {
  email: string;
  isValid: boolean;
}

export interface IAddEmailsPayload {
  emails: string[];
}

export interface IDeleteEmailPayload {
  email: string;
}

export interface IChangedEmailsPayload {
  emails: IEmailItem[];
}
