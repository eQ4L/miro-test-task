import { IEmailItem } from '~types';
import { ValidatorService } from '~services';

class EmailService {
  private _emails: IEmailItem[] = [];

  public getEmails(): IEmailItem[] {
    return this._emails;
  }

  public addEmails = (data: string | string[]) => {
    if (Array.isArray(data)) {
      data.map(email => {
        this._emails.push({
          isValid: ValidatorService.validateEmail(email),
          email
        });
      });

      return;
    }

    const emailItem = {
      isValid: ValidatorService.validateEmail(data),
      email: data
    };

    this._emails.push(emailItem);
  };

  public deleteEmail = (email: string) => {
    this._emails = this._emails.filter(el => el.email !== email);
  };
}

export default EmailService;
