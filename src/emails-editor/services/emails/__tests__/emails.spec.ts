import EmailService from '../index';

const service = new EmailService();
const testMail = 'test@test.com';

describe('EmailsService', () => {
  it('Get emails correctly', () => {
    const emails = service.getEmails();
    expect(emails).toStrictEqual([]);
  });

  it('Add emails correctly', () => {
    const expectedEmailList = [{ email: testMail, isValid: true }];
    service.addEmails(testMail);
    expect(service.getEmails()).toStrictEqual(expectedEmailList);
  });

  it('Correctly delete email', () => {
    service.deleteEmail(testMail);

    const emails = service.getEmails();

    expect(emails).not.toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          email: testMail
        })
      ])
    );
  });
});
