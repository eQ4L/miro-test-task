export type EventType = string;
export type Handler<T = any> = (event?: T) => void;
export type EventHandlerList = Array<Handler>;
export type EventHandlerMap = Map<EventType, EventHandlerList>;

export interface IEmitter {
  on<T = any>(type: EventType, handler: Handler<T>): void;
  off<T = any>(type: EventType, handler: Handler<T>): void;
  emit<T = any>(type: EventType, event?: T): void;
}

export default class Emitter implements IEmitter {
  private static instance: Emitter;
  private _handlers: EventHandlerMap;

  private constructor() {
    this._handlers = new Map();
  }

  public static getInstance = (): Emitter => {
    if (!Emitter.instance) {
      Emitter.instance = new Emitter();
    }

    return Emitter.instance;
  };

  public on<T = any>(type: EventType, handler: Handler<T>): void {
    const handlers = this._handlers.get(type);
    const isAdded = handlers && handlers.push(handler);

    if (!isAdded) {
      this._handlers.set(type, [handler]);
    }
  }

  public off<T = any>(type: EventType, handler: Handler<T>): void {
    const handlers = this._handlers.get(type);

    /**
     * TODO off handlers
     */
  }

  public emit<T = any>(type: EventType, event?: T): void {
    ((this._handlers.get(type) || []) as EventHandlerList).map(handler => handler(event));
  }
}
