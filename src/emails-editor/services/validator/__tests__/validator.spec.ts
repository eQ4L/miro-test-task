import ValidatorService from '../index';

describe('ValidatorService', () => {
  it('Correctly validate email string', () => {
    const validMail = 'test@test.com';
    const invalidMail = 'invalid@d.g';
    const checkedValidEmail = ValidatorService.validateEmail(validMail);
    const checkedInvalidEmail = ValidatorService.validateEmail(invalidMail);

    expect(checkedValidEmail).toBe(true);
    expect(checkedInvalidEmail).toBe(false);
  });
});
