const add = 'emails-add';
const change = 'emails-change';
const deleteItem = 'emails-delete';
const deleteLastItem = 'emails-delete-last';

export { add, change, deleteItem, deleteLastItem };
