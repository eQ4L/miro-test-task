import { IEmailItem, IDeleteEmailPayload, IAddEmailsPayload } from '~types';
import { EmailService, EventsService } from '~services';
import { Handler } from './services/events';
import { EVENTS, EMAILS_SEPARATOR } from './consts';
import View from './view';

const eventsService = EventsService.getInstance();
const emailsService = new EmailService();

interface IEmailsEditor {
  getEmails: () => IEmailItem[];
  addEmails: (data: string | string[]) => void;
  addEmailsListener: (fn: Function) => void;
  removeEmailsListener: (fn: Function) => void;
}

class EmailsEditor implements IEmailsEditor {
  private readonly container: HTMLElement;
  private view: HTMLElement | undefined;

  /**
   * @class EmailsEditor
   * @constructor
   * @param {HTMLElement} container Entry html element
   */
  constructor(container: HTMLElement) {
    this.container = container;

    this.exec();
  }

  /**
   * Runs render app, binds events etc.
   */
  private exec = () => {
    if (!this.container) {
      throw Error('No container provided!');
    }

    eventsService.on(EVENTS.ADD_EMAIL, this.onAddedEmails);
    eventsService.on(EVENTS.DELETE_EMAIL, this.onDeleteEmail);

    this.view = document.createElement(View.is);
    this.container.appendChild(this.view);
  };

  /**
   * A callback function for handle adding emails from view
   * @param emails { IAddEmailsPayload }
   */
  private onAddedEmails: Handler = ({ emails }: IAddEmailsPayload): void => {
    emailsService.addEmails(emails);

    eventsService.emit(EVENTS.EMAILS_CHANGED, { emails: emailsService.getEmails() });
  };

  /**
   * A callback function for handle deleting emails from view
   * @param email { IAddEmailsPayload }
   */
  private onDeleteEmail: Handler = ({ email }: IDeleteEmailPayload): void => {
    emailsService.deleteEmail(email);
    const emails = emailsService.getEmails();

    eventsService.emit(EVENTS.EMAILS_CHANGED, { emails });
  };

  /**
   * Returns all emails from instance
   */
  public getEmails = (): IEmailItem[] => emailsService.getEmails();

  /**
   * Allows add email or emails list to instance
   */
  public addEmails = (data: string | Array<string>): void => {
    if (Array.isArray(data)) {
      eventsService.emit(EVENTS.ADD_EMAIL, { emails: data });
    } else {
      const emails = data.split(EMAILS_SEPARATOR);
      eventsService.emit(EVENTS.ADD_EMAIL, { emails });
    }
  };

  /**
   * Allows subscribe to emails changes (add, remove, etc)
   * @param {Function} fn Callback function
   */
  public addEmailsListener = (fn: Function) =>
    eventsService.on(EVENTS.EMAILS_CHANGED, fn as Handler<Function>);

  /**
   * Allows unsubscribe from emails changes (add, remove, etc)
   * @param {Function} fn Callback function
   */
  public removeEmailsListener = (fn: Function) =>
    eventsService.off(EVENTS.EMAILS_CHANGED, fn as Handler<Function>);
}

window.EmailsEditor = EmailsEditor;

export default EmailsEditor;
