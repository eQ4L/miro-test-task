const template = (document.createElement('template').innerHTML = `
  <style>
    .emails-editor__item {
      position: relative;
      padding: 0 7px 0 10px;
      margin: 0 4px 3px 0;
      max-width: 100%;
      box-sizing: border-box;
      display: inline-flex;
      height: 24px;
      line-height: 24px;
      border-radius: 12px;
      font-size: 14px;
    }
    .emails-editor__item--valid {
      background-color: rgba(102, 152, 255, 0.2);
    }
    .emails-editor__item--invalid .emails-editor__title {
      border-bottom: 1px dashed #d14836;
    }
    .emails-editor__title {
      padding-right: 9px;
    }
    .emails-editor__delete {
      display: inline-block;
      font-size: 17px;
      font-weight: lighter;
      vertical-align: middle;
      cursor: pointer;
    }
    .emails-editor__delete:before {
      content: '\\D7';
    }
  </style>
  <div class="emails-editor__item">
    <div class="emails-editor__title"></div>
    <div class="emails-editor__delete"></div>
  </div>
`);

export default template;
