import { IEmailItem } from '~types';
import { EventsService } from '~services';
import { EVENTS } from '../../consts';
import template from './template';

const eventsService = EventsService.getInstance();

const VALID_CLASSNAME = 'emails-editor__item--valid';
const INVALID_CLASSNAME = 'emails-editor__item--invalid';

export class EmailsEditorItem extends HTMLElement {
  static readonly is = 'emails-editor-item';
  static readonly template = template;

  /**
   * Values
   */
  private _emailItem: IEmailItem | undefined;

  /**
   * Elements
   */
  private readonly $el: HTMLDivElement;
  private readonly $elTitle: HTMLDivElement;
  private readonly $deleteButton: HTMLDivElement;

  constructor() {
    super();

    const shadowRoot = this.attachShadow({ mode: 'open' });
    shadowRoot.innerHTML = EmailsEditorItem.template;

    this.$el = shadowRoot.querySelector('.emails-editor__item') as HTMLDivElement;
    this.$elTitle = shadowRoot.querySelector('.emails-editor__title') as HTMLDivElement;
    this.$deleteButton = shadowRoot.querySelector('.emails-editor__delete') as HTMLDivElement;
  }

  private connectedCallback() {
    this.$deleteButton.addEventListener('click', this.onDeleteClick);

    this.render();
  }

  private onDeleteClick = (e: Event) => {
    if (this._emailItem) {
      eventsService.emit(EVENTS.DELETE_EMAIL, { email: this._emailItem.email });
    }
  };

  private handleClassName = (): string => {
    if (!this._emailItem) {
      return '';
    }

    if (this._emailItem.isValid) {
      return VALID_CLASSNAME;
    }

    return INVALID_CLASSNAME;
  };

  private static get observedAttributes() {
    return ['email'];
  }

  private attributeChangedCallback(name: any, oldValue: any, newValue: any) {
    if (name === 'email') {
      this._emailItem = JSON.parse(newValue);
    }
  }

  private render = () => {
    if (this._emailItem) {
      this.$el.classList.add(this.handleClassName());
      this.$elTitle.innerHTML = this._emailItem.email;
    }
  };
}

customElements.define(EmailsEditorItem.is, EmailsEditorItem);
