import { IChangedEmailsPayload, IEmailItem } from '~types';
import { EventsService } from '~services';
import { Handler } from '../services/events';
import { EVENTS, EMAILS_SEPARATOR } from '../consts';
import { EmailsEditorItem } from './email-item';
import template from './template';

const eventsService = EventsService.getInstance();

const ENTER_KEY = 'Enter';
const BACKSPACE_KEY = 'Backspace';
const INSERT_INPUT_TYPE = 'insertFromPaste';

export default class EmailsEditorApp extends HTMLElement {
  static readonly is = 'emails-editor';
  static readonly template = template;

  /**
   * Values
   */
  private _emails: IEmailItem[] = [];

  /**
   * Elements
   */
  private readonly $emailsEditor: HTMLElement;
  private readonly $emailsList: HTMLElement;
  private readonly $emailsInput: HTMLInputElement;

  constructor() {
    super();
    const shadowRoot = this.attachShadow({ mode: 'open' });

    shadowRoot.innerHTML = EmailsEditorApp.template;

    this.$emailsEditor = shadowRoot.querySelector('.emails-editor') as HTMLElement;
    this.$emailsList = shadowRoot.querySelector('slot') as HTMLElement;
    this.$emailsInput = shadowRoot.querySelector('.emails-editor__item input') as HTMLInputElement;
  }

  private connectedCallback() {
    this.$emailsEditor.addEventListener('click', this.onEditorClicked);
    this.$emailsInput.addEventListener('change', this.onChange);
    this.$emailsInput.addEventListener('keydown', this.onKeyDown);
    this.$emailsInput.addEventListener('input', this.onInput);

    eventsService.on(EVENTS.EMAILS_CHANGED, this.onEmailsChanged);
  }

  private onEditorClicked = (e: any) => {
    if (e.target.className === this.$emailsEditor.className) {
      this.$emailsInput.focus();
    }
  };

  private onEmailsChanged: Handler = ({ emails }: IChangedEmailsPayload): void => {
    this._emails = emails;
    this.render();
  };

  private onKeyDown = (e: KeyboardEvent) => {
    if (e.key === ENTER_KEY || e.key === EMAILS_SEPARATOR) {
      this.addEmail();
      e.preventDefault();
    }

    if (
      this._emails.length > 0 &&
      this.$emailsInput.value.length === 0 &&
      e.key === BACKSPACE_KEY
    ) {
      const lastEmail = this._emails[this._emails.length - 1];
      eventsService.emit(EVENTS.DELETE_EMAIL, { email: lastEmail.email });

      e.preventDefault();
    }
  };

  private onInput = (e: any) => {
    if (e.inputType === INSERT_INPUT_TYPE) {
      this.addEmail();
    }
  };

  private onChange = () => this.addEmail();

  private addEmail = () => {
    const value = this.$emailsInput.value;

    if (value.length === 0) {
      return;
    }

    const emails = value.split(EMAILS_SEPARATOR);
    this.$emailsEditor.scrollTop = this.$emailsEditor.scrollHeight;
    this.$emailsInput.value = '';

    eventsService.emit(EVENTS.ADD_EMAIL, { emails });
  };

  private render = () => {
    if (!this.$emailsList) {
      return;
    }

    this.$emailsList.innerHTML = '';
    this._emails.map(emailItem => {
      const $emailItem = document.createElement(EmailsEditorItem.is);
      $emailItem.setAttribute('email', JSON.stringify(emailItem));
      this.$emailsList.appendChild($emailItem);
    });
  };
}

customElements.define(EmailsEditorApp.is, EmailsEditorApp);
