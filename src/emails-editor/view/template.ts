const template = (document.createElement('template').innerHTML = `
  <style>
    .emails-editor {
      height: 85px;
      padding: 7px 8px;
      background-color: #ffffff;
      border-top: 1px solid #d0d0d0;
      overflow-x: hidden;
      overflow-y: auto;
    }
    
    .emails-editor__item {
      padding: 0 7px;
      max-width: 100%;
      box-sizing: border-box;
      display: inline-flex;
      height: 24px;
      line-height: 24px;
      border-radius: 12px;
      font-size: 14px;
    }
    
    .emails-editor__item input {
      width: 100%;
      font-size: inherit;
      border: 1px solid transparent;
      background: transparent;
      outline: none;
    }
  </style>
  <div class="emails-editor">
    <slot></slot>
    <div class="emails-editor__item">
      <input type="text" placeholder="add more people..." />
    </div>
  </div>
`);

export default template;
