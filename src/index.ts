import { randomString } from '~utils';

const container = document.querySelector('#emails-editor') as HTMLElement;
const ee = new window.EmailsEditor(container);

const $addEmailButton = document.querySelector('.js-add-email');

if ($addEmailButton) {
  const onAddButtonClick = (): void => {
    const email = `${randomString()}@test.com`;
    ee.addEmails(email);
  };

  $addEmailButton.addEventListener('click', onAddButtonClick);
}

const $emailCountButton = document.querySelector('.js-email-count');

if ($emailCountButton) {
  const onEmailCountClick = () => {
    const emails = ee.getEmails();
    const validEmailsCount = emails.filter(email => email.isValid).length;
    alert(validEmailsCount);
  };

  $emailCountButton.addEventListener('click', onEmailCountClick);
}
