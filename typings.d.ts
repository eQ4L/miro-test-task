import EmailsEditor from './src/emails-editor';

declare global {
  interface Window {
    EmailsEditor: typeof EmailsEditor;
  }
}
