const CopyWebpackPlugin = require('copy-webpack-plugin');
const { CheckerPlugin } = require('awesome-typescript-loader');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const paths = require('./paths');

module.exports = {
  entry: {
    'emails-editor': paths.src + '/emails-editor',
    app: paths.src
  },
  output: {
    path: paths.dist,
    filename: '[name].js'
  },
  resolve: {
    extensions: ['.js', '.ts', '.tsx', 'scss'],
    alias: {
      '~services': paths.src + '/emails-editor/services',
      '~types': paths.src + '/emails-editor/types.ts',
      '~utils': paths.src + '/utils'
    }
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'awesome-typescript-loader',
        exclude: [/node_modules/]
      }
    ]
  },
  plugins: [
    new CopyWebpackPlugin(),
    new CheckerPlugin(),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: paths.public + '/index.html'
    })
  ]
};
