const path = require('path');

module.exports = {
  dist: path.resolve(__dirname, '../dist'),
  public: path.resolve(__dirname, '../public'),
  src: path.resolve(__dirname, '../src')
};
