
[Demo](http://eq4l.gitlab.io/miro-test-task)

## Requirements
- `node v10.16.2`
- `yarn`

## Install
```
$ yarn
```

## Usage
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- some attrs -->
</head>
<body>
    <!-- your code -->
    <div id="container"></div>
    <script src="/emails-editor.js" type="text/javascript"></script>
    <script type="text/javascript">
        const container = document.querySelector('#container');
        const ee = new EmailsEditor(container);
    </script>
</body>
</html>
```

### API
*EmailsEditor* supports methods for manipulate data

### Interfaces
```ts
interface IEmailItem {
    email: string;
    isValid: boolean;
}
```

#### Methods

`getEmails: () => IEmailItem[]` this function is used for getting list of emails
##### example
```js
const container = document.querySelector('#container');
const ee = new EmailsEditor(container);
ee.getEmails() // [{email: 'test@mail.ru', isValid: true}, ...]
```

`addEmails: (data: string | string[]) => void` function is used for set email(s)
##### example
```js
const container = document.querySelector('#container');
const ee = new EmailsEditor(container);
const emails = ['test@mail.ru'];
ee.addEmails(emails);
```

`addEmailListener: (fn: Function) => void` method allows to subscribe for emails changed
##### example
```ts
const container = document.querySelector('#container') as HTMLElement;
const ee = new EmailsEditor(container);
const onEmailsChanged = (event: CustomEvent) => console.log(event.detail.emails);
ee.addEmailListener(onEmailsChanged);
```

`removeEmailListener: (fn: Function) => void` method allows to unsubscribe for emails changed
##### example
```ts
const container = document.querySelector('#container') as HTMLElement;
const ee = new EmailsEditor(container);
const onEmailsChanged = (event: CustomEvent) => console.log(event.detail.emails);
ee.removeEmailListener(onEmailsChanged);
```